## This is a fork of [https://github.com/jmichas/PlexTrainingVideoScannerAndAgent](https://github.com/jmichas/PlexTrainingVideoScannerAndAgent), where I further modify, improve and automate the Plex scanner and metadata processing for Lynda, Udemy and other tutorial types, processing metadata from JSON and TXT files produced from my other tools/repos in the common project [TutorialsMetaScripts](https://bitbucket.org/lv-zOne/workspace/projects/TUT) (see [udemy-process](https://bitbucket.org/lv-zOne/udemy-process/) and [lynda-dl-tweaks](https://bitbucket.org/lv-zOne/lynda-dl-tweaks))

## None of my tools downloads anything you're not supposed to have access to, they all use public APIs JSON/XML/TXT, file and folder processing techniques. All the processing work is done on training content that has been purchased and downloaded for personal use. 

### The main purpose of this project is to make use of the produced file/folder structure of Lynda Tutorials - as downloaded by [lynda-dl-tweaks](https://bitbucket.org/lv-zOne/lynda-dl-tweaks) and Udemy Tutorials as downloaded by [Udeler](https://github.com/FaisalUmair/udemy-downloader-gui) and metadata processed and saved to files further by [udemy-process](https://bitbucket.org/lv-zOne/udemy-process/).

## The orignal tool I forked from already successfully processes tutorials as Plex TV Series, where chapters are treated as Seasons and Episodes, well are the videos (episodes) in each Chapter. What I've done on top of that is:

### Tweak the scripts to:

* handle the folder and file patterns that I have as a result of the tools I've used (as mentioned above) to download and save courses, chapters, episodes such as
    * the 2 dates I put in folder names for **Lynda** tutorials - 'Lynda-**20200529-[20190617]**-Learning.Zoom-[Garrick.Chow]-[01h21m35s]', which were breaking the original tool processing
    * '-metadata.json' file pattern in **Lynda** files like 'Learning.Zoom-metadata.json'
    * '_course.json' file pattern in **Udemy** files like 'become_an_algebra_master_course.json'. Udemy folders also have '_curriculum.json' files, but their metadata might be used later for episodes but not main course
* process the different JSON and TXT files produced by those other tools so that Plex Courses (TV Series), Plex Chapters (TV Series Seasons) and Plex Episodes (TV Series Episodes) get extra metadata - such as Summary, Tagline, Date created (TV Series First Aired) - author info merged into metadata summary
* Cover images (thumbs) used for Lynda and Udemy
* Process tags for Lynda and generate tags for Udemy from course title, stripping stop words and non-alpha characters


## In other words I take folders like these below to produce the result

### Lynda & Udemy folder patterns

![](images/folders.png?raw=true)

### Lynda folder contents / file patterns

![](images/lynda_folder_example.png?raw=true)

### Udemy folder contents / file patterns

![](images/udemy_folder_example.png?raw=true)

### Result in Plex

#### Summary View

![](images/plex_summary_view.png?raw=true)

#### List View

![](images/plex_list_view.png?raw=true)

#### Course View

![](images/plex_course_view.png?raw=true)

#### Course View - Edit Details

![](images/plex_course_edit_details.png?raw=true)

#### Course View - Edit Details - Tags

![](images/plex_course_details_tags.png?raw=true)

#### Course View - Edit Details - Poster

![](images/plex_course_details_poster.png?raw=true)

#### Course - Season- Episodes View

![](images/plex_season_episodes.png?raw=true)


## See below original README

---



# PlexTrainingVideoScannerAndAgent
A Plex scanner and agent for training videos i.e. Pluralsight, Lynda.com, Udemy, etc.

![](images/main.png?raw=true)

This currently only works with Pluralsight, Udemy and Lynda.com offline files. The structure of the files is somewhat rigid, but it is pretty much how all of my files were organized.

Course Name/1. Chapter Name/Media File<br/>
i.e. Lynda.com.Angular2.for.NET.Developers\1. Course Overview\500547_01_02_XR15_SampleMovieDb.mp4

I have ignored the Exercise Files, Source Files, and Files directories so that they can be stored with the video files but not interfere with Plex scanning.

THIS IS A WORK IN PROGRESS!!<br/>
I obviously haven't accounted for every permutation of how you may have these files organized so as we all discover new patterns we can update the regex patterns to accomodate new permutations. Send a pull request if you add new regex patterns.

NO ARTWORK IS DOWNLOADED!!<br/>
So, this gets no information from any training site, there seemed to be no good way to locate show/course data, your best bet at this point is to manually go take a screen shot and add it as a poster in Plex.

The only automated thing the code does is use the Pluralsight, Udemy or Lynda.com in the folder name for the show to set the Network/Studio in Plex, this allows you to see all videos from each provider if you want.

AUTOMATIC SEASON NAMING!!<br/>
One really cool thing this does is use the section/chapter folder name as the season title. Previously it seemed there was no way to set the season title but I figured out a little hack to call into the Plex UI to set it. While it is cool, it makes the code super duper brittle, if Plex decides to change the UI it may break my code, but I'm willing to take that chance ;)

![](images/course.png?raw=true)

Also, finally, this is my first Python anything, I don't "know" the language, so if there are syntactical improvements please let me know. I felt like I was back doing VBScript (sorry python peeps!) trying to fumble through this.

# Installation
Download the files or clone the repo.<br/>
Put the Series directory with the Training Video Scanner.py into the Scanner directory in your plex media server root directory/folder.<br/>
Rename the TrainingVideo-Agent_bundle to TrainingVideo-Agent.bundle and put it in the Plug-ins directory in your plex media server root directory/folder.<br/>

That's about it, now you just need to setup a new library as a TV Show, in the advanced settings set the Scanner to Training Video Scanner and the agent to Training Video Agent. You will need to also enter your plex auth token. If you are unsure how to find your plex token google is your friend.
